# Are Cloud Detection U-Nets Robust Against In-Orbit Image Acquisition Conditions?

**Bartosz Grabowski, Maciej Ziaja, Michal Kawulok, Marcin Cwiek, Tomasz Lakota, Nicolas Longépé, and Jakub Nalepa**

(Accepted to IGARSS 2022)

## Test scenes from the Landsat 8 Cloud Cover Assessment Validation Data

The following scenes from the Landsat 8 Cloud Cover Assessment Validation Data (https://landsat.usgs.gov/landsat-8-cloud-cover-assessment-validation-data) were chosen to our test set:

- **Barren:**
LC80420082013220LGN00 and
LC81990402014267LGN00

- **Forest:**
LC80070662014234LGN00 and
LC82290572014141LGN00
 
- **Grass/Crops:**
LC80290372013257LGN00 and 
LC81510262014139LGN00

- **Shrubland:**
LC80010732013109LGN00 and 
LC80630152013207LGN00

- **Snow/Ice:**
LC80211222013361LGN00 and 
LC80250022014232LGN00

- **Urban:**
LC80640452014041LGN00 and 
LC81180382014244LGN00
 
- **Water:**
LC80180082014215LGN00 and 
LC81240462014238LGN00

- **Wetlands:**
LC81580172013201LGN00 and 
LC81080182014238LGN00
